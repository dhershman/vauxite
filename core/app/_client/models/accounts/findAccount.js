'use strict';

function findAccount(options) {
	var defaults = {
		type: 'GET',
		asyn: true,
		url: '/resource/findAccount',
		dataType: 'JSON',
		contentType: 'text/json',
		processData: true
	};
	var data = {};
	var def = $.Deferred();
	var opts = $.extend({}, defaults, options);

	function fetch(args) {
		$.ajax({
			type: opts.type,
			async: opts.async,
			url: opts.url,
			data: JSON.stringify(args),
			dataType: opts.dataType,
			processData: opts.processData,
			success: success,
			error: error
		});
		return def;
	}

	function success(d) {
		setData(d);
		def.resolve(d);
	}

	function error(e) {
		def.reject(e);
	}

	function setData(d) {
		data = d;
	}

	function getData() {
		return data;
	}

	return {
		getData: getData,
		setData: setData,
		fetch: fetch
	};

}

module.exports = findAccount;