'use strict';

function updateAccount(options) {
	var defaults = {
		type: 'POST',
		async: true,
		url: '/resource/updateAccount',
		dataType: 'JSON',
		contentType: 'application/json; charset=UTF-8',
		traditional: true,
		processData: true
	};
	var data;
	var opts = $.extend({}, defaults, options);
	var def = $.Deferred();

	function fetch(args) {
		$.ajax({
			type: opts.type,
			async: opts.async,
			url: opts.url,
			data: {info: JSON.stringify(args)},
			dataType: opts.dataType,
			processData: opts.processData,
			success: success,
			error: error
		});
		return def;
	}

	function success(d) {
		setData(d);
		def.resolve(data);
	}

	function error(e) {
		def.reject(e);
	}

	function setData(d) {
		data = d;
	}

	function getData() {
		return data;
	}
	return {
		getData: getData,
		setData: setData,
		fetch: fetch
	};

}

module.exports = updateAccount;