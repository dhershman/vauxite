'use strict';

function downloadFile(options) {
	var default = {
		type: 'GET',
		url: '/resource/download-file'
	};
	var data;
	var def = $.Deferred();
	var opts = $.extend({}, default, options);

	function fetch(args) {
		var xhr = new XMLHttpRequest();

		var oURL = opts.url + '?searchablename=' + args.searchablename;
		xhr.open(opts.type, oURL, true);
		xhr.onload = function() {
			if (xhr.status >= 200 && xhr.status < 400) {
				data = JSON.parse(xhr.responseText);
				success(data);
			} else {
				console.log('Error');
				error();
			}
		};
		xhr.onerror = function(e) {
			console.log('connection error');
			error(e);
		};
		xhr.send();
		return def;
	}

	function success(d) {
		setData(d);
		def.resolve(d);
	}

	function error(e) {
		def.reject(e);
	}

	function setData(d) {
		data = d;
	}

	function getData() {
		return data;
	}

	return {
		setData: setData,
		getData: getData,
		fetch: fetch
	};
}