/*Handle calls out to steam api to grab news on apps.*/
'use strict';
function getMods(options) {
	var defaults = {
		type: 'GET',
		async: true,
		url: '/resource/get-mods',
		dataType: 'JSON',
		contentType: 'text/json',
		processData: true
	};
	var data;
	var opts = $.extend({}, defaults, options);
	var def = $.Deferred();

	function fetch(args) {
		var request = new XMLHttpRequest();

		var oURL = buildRequest(args);
		request.open(opts.type, oURL, true);
		request.onload = function() {
			if (request.status >= 200 && request.status < 400) {
				data = JSON.parse(request.responseText);
				success(data);
			} else {
				console.log('Error');
				error();
			}
		};
		request.onerror = function(e) {
			console.log('connection error');
			error(e);
		};
		request.send();
		return def;
	}

	function buildRequest(args) {
		var oURL = opts.url;
		var count = 0;
		for (var o in args) {
			if (count <= 0) {
				oURL += '?'+o+'='+args[o];
			} else {
				oURL += '&'+o+'='+args[o];
			}
			count++;
		}
		console.log(oURL);
		return oURL;
	}

	function success(d) {
		setData(d);
		def.resolve(data);
	}

	function error(e) {
		def.reject(e);
	}

	function setData(d) {
		data = d;
	}

	function getData() {
		return data;
	}
	return {
		getData: getData,
		setData: setData,
		fetch: fetch
	};
}

module.exports = getMods;
