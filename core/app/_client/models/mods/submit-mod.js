'use strict';

function submitMod(options) {
	var defaults = {
		type: 'POST',
		async: true,
		url: '/resource/submit-mod',
		dataType: 'JSON',
		contentType: 'text/json',
		traditional: true,
		processData: true
	};
	var data;
	var opts = $.extend({}, defaults, options);
	var def = $.Deferred();

	function fetch(args) {
		$.ajax({
			type: opts.type,
			async: opts.async,
			url: opts.url,
			data: JSON.stringify(args),
			dataType: opts.dataType,
			processData: opts.processData,
			success: success,
			error: error
		});
		return def;
	}

	function success(d) {
		setData(d);
		def.resolve(data);
	}

	function error(e) {
		def.reject(e);
	}

	function setData(d) {
		data = d;
	}

	function getData() {
		return data;
	}
	return {
		getData: getData,
		setData: setData,
		fetch: fetch
	};
}

module.exports = submitMod;
