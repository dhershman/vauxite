/*Handle calls out to steam api to grab news on apps.*/
'use strict';
function steamNews(options) {
	var defaults = {
		type: 'GET',
		async: true,
		dataType: 'json',
		contentType: 'application/json',
		url: '/steam/newsAPI'
	};
	var data = [];
	var opts = $.extend({}, defaults, options);
	var def = $.Deferred();

	function fetch(args) {
		$.ajax({
			type: opts.type,
			async: opts.async,
			url: opts.url,
			contentType: opts.contentType,
			dataType: opts.dataType,
			data: args,
			success: success,
			error: error
		});
		return def;
	}

	function success(d) {
		setData(d);
		def.resolve(data);
	}

	function error(err) {
		def.reject(err);
	}


	function setData(d) {
		data.push(d.appnews.newsitems[0]);
	}

	function getData() {
		return data;
	}

	return {
		setData: setData,
		getData: getData,
		fetch: fetch
	};
}


module.exports = steamNews;
