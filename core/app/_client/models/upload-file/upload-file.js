/*Handle calls out to steam api to grab news on apps.*/
'use strict';

function uploadFile(options) {
	var defaults = {
		type: 'POST',
		url: '/resource/upload-file'
	};
	var data;
	var opts = $.extend({}, defaults, options);
	var def = $.Deferred();

	function fetch(args) {
		console.log(args);
		$.ajax({
			url: opts.url,
			type: opts.type,
			data: args,
			processData: false,
			contentType: false,
			success: success,
			error: error,
			xhr: handleProgress
		});
		return def;
	}

	function success(d) {
		setData(d);
		def.resolve(data);
	}

	function error(e) {
		def.reject(e);
	}

	function setData(d) {
		data = d;
	}

	function getData() {
		return data;
	}

	function handleProgress() {
		var xhr = new XMLHttpRequest();
		xhr.upload.addEventListener('progress', function(evt) {
			if (evt.lengthComputable) {
				var percentComplete = evt.loaded / evt.total;
				percentComplete = parseInt(percentComplete * 100);
				$('.progress-bar').text(percentComplete + '%');
				$('.progress-bar').width(percentComplete + '%');

				if (percentComplete === 100) {
					$('.progress-bar').html('Done');
				}
			}
		}, false);

		return xhr;
	}

	return {
		getData: getData,
		setData: setData,
		fetch: fetch,
		handleProgress: handleProgress
	};
}

module.exports = uploadFile;
