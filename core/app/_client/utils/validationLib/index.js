'use strict';

var validationLib = function(options) {
	this.defaultRules = {
		type: null
		,length: null
		,minlength: null
		,maxlength: null
		,required: true
		,matchagainst: null
		,basepattern: null
		,startpattern: null
		,endpattern: null
		,negatepattern: /[^a-z0-9_\-\*\'$#!@\?\.,\s\u00c0-\u017f]/ig
		,mustcontainletters: false
		,mustcontainupperandlowercaseletters: false
		,mustcontainnumbers: false
		,mustcontainspecialcharacters: false
		,mustcontainspecialcharactersornumbers: false
	};

	this.rules = $.extend(this.defaultRules, options);
};

validationLib.prototype = {

	isValid: function(val, args) {
		var trimmed = $.trim(val);
		var result = '';
		var validationRules = $.extend({}, this.rules);
		var curRule;
		var curMethod;
		if (args.rule != null  && this.types[args.rule] != null){
				validationRules = $.extend(validationRules, this.types[args.type]);
			}

		if (validationRules.required === true || trimmed.length > 0) {
			for (var i = this.validationMethods.length - 1; i > -1; i--){
				curRule = validationRules[this.validationMethods[i].attr];

				curMethod = this.validationMethods[i].name;
				if (curRule !== false && curRule != null){
					result = this[curMethod](trimmed, curRule);
					if (result.isValid === false){
						return result;
					}
				}
			}
		}
		return {isValid: true, errorMessage: ''};
	}
	//checks for an exact length
	/**
	 * Method to check if value is an acceptable length
	 * @method   isCorrectLength
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}          val     string containing the consumer entered information
	 * @param    {Number}          curRule a number based on the required amount of letters/numbers for the current field
	 * @return   {Boolean}                 returns a boolean based on if the length is correct or not
	 */
	,isCorrectLength: function(val, curRule){
		if (val.length !== Number(curRule)){
			return {isValid: false, errorMessage: 'length'};
		}
		return {isValid: true};
	}

	//ensures string has length equal to or greater than min length
	/**
	 * Method to check if a value is too short
	 * @method   isNotTooShort
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}          val     string containing the consumer entered information
	 * @param    {Number}          curRule number based on the length of the current value
	 * @return   {Boolean}                 returns a boolean based on if the information is too short or not
	 */
	,isNotTooShort: function(val, curRule){
		if (val.length < Number(curRule)){
			return {isValid: false, errorMessage: 'minLength'};
		}
		return {isValid: true};
	}

	//ensures string has length equal to or less than max length
	/**
	 * Method to check if a value is too long
	 * @method   isNotTooLong
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}          val     string containing consumer entered information
	 * @param    {Number}          curRule number based on the length of the current value
	 * @return   {Boolean}                 returns a boolean based on if the information is too long or not
	 */
	,isNotTooLong: function(val, curRule){
		if (val.length > Number(curRule)){
			return {isValid: false, errorMessage: 'maxLength'};
		}
		return {isValid: true};
	}

	//ensure string has length
	/**
	 * Method to check if a value exists
	 * @method   hasValue
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}          val string containing consumer entered information
	 * @return   {Boolean}             returns a boolean based on if the consumer has entered information or not
	 */
	,hasValue: function(val){
		if (val.length === 0){
			return {isValid: false, errorMessage: 'required'};
		}
		return {isValid: true};
	}

	//matches string against another string
	/**
	 * Method to match a string against another string
	 * @method   doesMatchGivenValue
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}            val     string containing consumer entered value
	 * @param    {Number}            curRule number to compare the current value against the rule
	 * @return   {Boolean}                    returns a boolean of if the strings match or not
	 */
	,doesMatchGivenValue: function(val, curRule){
		if (val !== $.trim(curRule)){
			return {isValid: false, errorMessage: 'matchAgainst'};
		}
		return {isValid: true};
	}

	//matches string against regex
	/**
	 * Method to compare string against regex
	 * @method   matchesBasePattern
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}           val     string containing consumer entered value
	 * @param    {Number}           curRule number of current rule for comparison
	 * @return   {Boolean}                   returns a boolean of if the value meets the regex or not
	 */
	,matchesBasePattern: function(val, curRule){
		if (val.search(new RegExp(curRule)) === -1){
			return {isValid: false, errorMessage: 'basePattern'};
		}
		return {isValid: true};
	}

	//ensures string does not match regex
	/**
	 * Method to ensure the string does not match regex
	 * @method   doesNotMatchNegatePattern
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}                  val     string containing a consumer entered value
	 * @param    {String}                  curRule a string of regex to compare the value against
	 * @return   {Boolean}                          returns a boolean based on if the string matched regex or not
	 */
	,doesNotMatchNegatePattern: function(val, curRule){
		if (val.search(new RegExp(curRule)) !== -1){
			return {isValid: false, errorMessage: 'negatePattern'};
		}
		return {isValid: true};
	}

	//ensures string has letters
	/**
	 * Method to ensure the string has letters
	 * @method   hasLetters
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}          val string containing the consumer entered value
	 * @return   {Boolean}             returns a boolean if the string has letters or not
	 */
	,hasLetters: function(val){
		if (val.search(/[a-z]/i) === -1){
			return {isValid: false, errorMessage: 'mustContainLetters'};
		}
		return {isValid: true};
	}

	//ensures string has upper and lower case letters
	/**
	 * Method to ensure the string has upper & lower case letters
	 * @method   hasUpperAndLowerCaseLetters
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String}                    val string containing the consumer entered information
	 * @return   {Boolean}                       returns a boolean based on if the value has upper & lower case letters or not
	 */
	,hasUpperAndLowerCaseLetters: function(val){
		if ((val.search(/[a-z]/) === -1) || (val.search(/[A-Z]/) === -1)){
			return {isValid: false, errorMessage: 'mustContainUpperAndLowerCaseLetters'};
		}
		return {isValid: true};
	}

	//ensures string has special characters
	/**
	 * Method to check if the string has special characters
	 * @method   hasSpecialCharacters
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String|Number}             val string or number containing consumer entered information
	 * @return   {Boolean}                returns a boolean based on if the string has special characters or not
	 */
	,hasSpecialCharacters: function(val){
		if ((val.search(/[^a-z0-9]/i) === -1)){
			return {isValid: false, errorMessage: 'mustContainSpecialCharacters'};
		}
		return {isValid: true};
	}

	//ensures string has numbers or special characters
	/**
	 * Method to check if the string has numbers or special characters
	 * @method   hasNumbersOrSpecialCharacters
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String|Number}                      val string or number containing consumer entered information
	 * @return   {Boolean}                         returns a boolean based on if the string has numbers or special characters
	 */
	,hasNumbersOrSpecialCharacters: function(val){
		if ((val.search(/[0-9]/) === -1) && (val.search(/[^a-z0-9]/i) === -1)){
			return {isValid: false, errorMessage: 'mustContainSpecialCharactersOrNumbers'};
		}
		return {isValid: true};
	}

	//ensures string has numbers
	/**
	 * Method to check if the string has numbers
	 * @method   hasNumbers
	 * @memberOf comp.model.ValidationLibrary
	 * @param    {String|Number}          val string or number containing consumer entered information
	 * @return   {Boolean}             returns a boolean based on if the string has numbers or not
	 */
	,hasNumbers: function(val){
		if (val.search(/[0-9]/) === -1){
			return {isValid: false, errorMessage: 'mustContainNumbers'};
		}
		return {isValid: true};
	}

	/**
	 * Method to check if the value is positive
	 * @method   isPositiveNumber
	 * @memberOf comp.model.ValidationLibrary
	 * @param  {String|Number}  val String or number containing consumer entered information
	 * @return {Boolean}     returns a boolean if the number passes as a number and as a positive number
	 */
	,isPositiveNumber: function(val) {
		if (!Number(val) || Number(val) < 0) {
			return {isValid: false, errorMessage: 'notPositiveNumber'};
		}
		return {isValid: true};
	}
};

validationLib.prototype.types = {
	name: {
		maxlength: 30
		,mustcontainletters: true
		,negatepattern: /[^\w\-\'\s\u00c0-\u017f]/ig
	}
	,fullName: {
		maxlength: 60
		,mustcontainletters: true
		,negatepattern: /[^\w\-\'\s\u00c0-\u017f]/ig
	}
	,version: {
		isNotTooShort: 5
		,isNotTooLong: 5
		,hasNumbers: true
		,basepattern: /([0-9])\.([0-9])\.([0-9])/ig
	}
	,email: {
		basepattern: /^[\w\u00c0-\u017f][\w\.\-\_\u00c0-\u017f]*[\w\u00c0-\u017f]+[@][\w\u00c0-\u017f][\w\.\-\_\u00c0-\u017f]*[\w\u00c0-\u017f]+[\.][a-z]{2,4}$/i
		,maxlength: 90
		,negatepattern: null
	}
	,address1: {
		maxlength: 60
		,mustcontainletters: true
	}
	,address2: {
		maxlength: 60
		,required: false
		,mustcontainletters: true
	}
	,city: {
		maxlength: 40
		,mustcontainletters: true
	}
	,genericNumber: {
		minlength: 1,
		mustcontainnumbers: true
	}
	,positiveNumber: {
		mustbepositivenumber: true
	}
	,zipCodeUS: {
		basepattern: /^\d{5}(-\d{4})?$/
		,negatepattern: null
	}
	,zipCodeCA: {
		basepattern: /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i
		,negatepattern: null
	}
	,zipCode: {
		basepattern: /(^\d{5}(-\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$)/i
		,negatepattern: null
	}
	,phoneNumber: {
		basepattern: /^[0-9]{10}$/g
		,negatepattern: null
	}
	,formattedPhoneNumber: {
		basepattern: /^[\(]?[0-9]{3}[\)\-\.]?[0-9]{3}[\-\.]?[0-9]{4}$/
		,negatepattern: null
	},
	multiplePhoneFormats: {
		basepattern: /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-‌​9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})$/
		,negatepattern: null
	}
	,VIN: {
		basepattern: /^[a-hj-npr-z0-9]{9}[a-hj-npr-tv-y1-9]{1}[a-hj-npr-z0-9]{7}$/i
		,negatepattern: null
	}
	,licensePlate: {
		basepattern: /^[a-z0-9\-\s]+$/i
		,maxlength: 10
		,negatepattern: null
	}
	,month: {
		basepattern: /(^1[0-2]$)|(^0?[1-9]$)/
		,negatepattern: null
	}
	,day: {
		basepattern: /(^0?[1-9]$)|(^[1-2][0-9]$)|(^3[0-1]$)/
		,negatepattern: null
	}
	,yearTwoDigit: {
		basepattern: /^[0-9]{2}$/
		,negatepattern: null
	}
	,yearFourDigit: {
		basepattern: /^[1-2]{1}[0-9]{3}$/
		,negatepattern: null
	}
	//mm-dd-yyyy
	,date: {
		basepattern: /^((1[0-2])|(0?[1-9]))[\-\/\.]?((0?[1-9])|([1-2][0-9])|(3[0-1]))[\-\/\.]?(([1-2]{1}[0-9]{3})|([0-9]{2}))$/
		,negatepattern: null
	}
	//yyyy-mm-dd
	,dateProper: {
		basepattern: /^(([1-2]{1}[0-9]{3})|([0-9]{2}))[\-\/\.]?((1[0-2])|(0?[1-9]))[\-\/\.]?((0?[1-9])|([1-2][0-9])|(3[0-1]))$/
		,negatepattern: null
	}
	,password: {
		minlength: 8
		,maxlength: 30
		,mustcontainletters: true
	}
	,passwordSimple: {
		minlength: 8
		,maxlength: 30
		,mustcontainupperandlowercaseletters: true
	}
	,passwordComplex:{
		minlength: 8
		,maxlength: 30
		,mustcontainupperandlowercaseletters: true
		,mustcontainspecialcharactersornumbers: true
	}
	,passwordSecure: {
		minlength: 8
		,maxlength: 30
		,mustcontainupperandlowercaseletters: true
		,mustcontainnumbers: true
		,mustcontainspecialcharacters: true
	}
	,visaCard: {
		basepattern: /^4[0-9]{15}$/
		,negatepattern: null
	}
	,masterCard: {
		basepattern: /^5[1-5][0-9]{14}$/
		,negatepattern: null
	}
	,americanExpressCard: {
		basepattern: /^3(4|7)[0-9]{13}$/
		,negatepattern: null
	}
	,cvn: {
		negatepattern: /[^0-9]/
		,length: 3
	}
	,cvnAmex: {
		negatepattern: /[^0-9]/
		,length: 4
	}
	,selectGroup: {
		negatepattern: /^\-$/
	}
	,number: {
		negatepattern: /[^0-9]/
	}
	,formattedNumber: {
		negatepattern: /[^0-9,.]/
		,mustcontainnumbers: true
	}
	,dot: {
		basepattern: /^(DOT)?[A-Z\d]{7,8}(([0-4]\d)|(5[0-3]))\d{2}$/i
		,negatepattern: null
	}
	,warrantyTreadDepth: {
		basepattern: /^(([0-1]?[0-9]|2[0-1])(\.[0-9])?|22)$/i
		,negatepattern: null
	}
};

validationLib.prototype.validationMethods = [
	{
		name: 'isCorrectLength'
		,attr: 'length'
	}
	,{
		name: 'isPositiveNumber'
		,attr: 'mustbepositivenumber'
	}
	,{
		name: 'isNotTooShort'
		,attr: 'minlength'
	}
	,{
		name: 'isNotTooLong'
		,attr: 'maxlength'
	}
	,{
		name: 'hasValue'
		,attr: 'required'
	}
	,{
		name: 'doesMatchGivenValue'
		,attr: 'matchagainst'
	}
	,{
		name: 'matchesBasePattern'
		,attr: 'basepattern'
	}
	,{
		name: 'doesNotMatchNegatePattern'
		,attr: 'negatepattern'
	}
	,{
		name: 'hasLetters'
		,attr: 'mustcontainletters'
	}
	,{
		name: 'hasUpperAndLowerCaseLetters'
		,attr: 'mustcontainupperandlowercaseletters'
	}
	,{
		name: 'hasNumbers'
		,attr: 'mustcontainnumbers'
	}
	,{
		name: 'hasSpecialCharacters'
		,attr: 'mustcontainspecialcharacters'
	}
	,{
		name: 'hasNumbersOrSpecialCharacters'
		,attr: 'mustcontainspecialcharactersornumbers'
	}
];

module.exports = new validationLib();