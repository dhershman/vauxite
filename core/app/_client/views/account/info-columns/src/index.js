'use strict';

function infoCols($els, utils, options) {
	var defaults = {
		template: '/assets/templates/views/account/info-panels.hbs'
	};

	var opts = $.extend({}, defaults, options);

	function render(d) {
		utils.getTemplate(opts.template, function(tmpl) {
			$els.html(tmpl(d));
		});
	}

	return {
		render: render
	};
}
module.exports = infoCols;
