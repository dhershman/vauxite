'use strict';

function titleBar($els, utils, options) {
	var defaults = {
		template: '/assets/templates/views/account/title-bar.hbs'
	};

	var opts = $.extend({}, defaults, options);

	function render(d) {
		utils.getTemplate(opts.template, function(tmpl) {
			$els.html(tmpl(d));
		});
	}

	return {
		render: render
	};
}
module.exports = titleBar;
