'use strict';

function homeJumboView($els, utils, options) {
    var defaults = {
        template: 'assets/templates/views/home/home-jumbo.hbs'
    };
    var opts = $.extend({}, defaults, options);

    function render(d) {
        utils.getTemplate(opts.template, function(tmpl) {
            $els.html(tmpl(d));
        });
    }

    function setGame(e) {
        var selected = $(e.currentTarget);
        var data = {
            game: {
                title: 'Stonehearth',
                desc: 'A Simulation game in which you run a town and manage the inhabitants to ensure their survival you must harvest food and fight off waves of monsters'
            }
        };
        render(data);
    }

    return {
        render: render,
        setGame: setGame
    };
}

module.exports = homeJumboView;
