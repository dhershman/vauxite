'use strict';

function homeMainView($els, models, utils, options) {
    var def = $.Deferred();
    var defaults = {
        template: '/assets/templates/views/home/home-main.hbs'
    };
    var opts = $.extend({}, defaults, options);
    function getNews(apps) {
            models.fetch({
                appid: apps,
                maxlength: 100,
                format: 'json',
                count: 1
            })
            .done(function(res) {
                console.log(res);
            })
            .fail(function(err) {
                console.log(err);
            });
        return models.getData();
    }

    function render() {
        var apps = getNews(['413150', '253250']);
        utils.getTemplate(opts.template, function(tmpl) {
           $els.html(tmpl({gameArticles: apps}));
        });
    }
    return {
        getNews: getNews,
        render: render
    };
}
module.exports = homeMainView;
