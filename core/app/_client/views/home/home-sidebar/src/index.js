'use strict';

function homeSidebarView($els, utils, options) {
    var defaults = {
        template: 'assets/templates/views/home/home-sidebar.hbs'
    };
    var opts = $.extend({}, defaults, options);

    function render(d) {
        utils.getTemplate(opts.template, function(tmpl) {
            $els.html(tmpl(d));
        });
    }
    return {
        render: render
    };
}

module.exports = homeSidebarView;
