'use strict';

function modInfoView($els, utils, models, options) {

	var defaults = {
		template: '/assets/templates/views/mod-details/mod-info.hbs',
		mod: {}
	};

	var opts = $.extend({}, defaults, options);
	render(opts.mod);

	function getInfo() {

	}

	function downloadMod() {

	}

	function render(d) {
		console.log(d);
		utils.getTemplate(opts.template, function(tmpl) {
			$els.html(tmpl(d[0])).hide().fadeIn('slow');
		});
	}

	return {
		render: render
	};
}

module.exports = modInfoView;