'use strict';

function modFiltering($els, utils, options) {
	var defaults = {
		template: '/assets/templates/views/modhub/modlistfilters.hbs',
		itemsToShow: 10,
		game: '',
		list: {}
	};
	var opts = $.extend({}, defaults, options);
	var filtersData = {
		returnCount: 10,
		returnOffset: 0
	};
	var tableOptions ={};
	render(opts);

	$els.on('comp_ui_filtering.input', handleInputs)
		.on('comp_ui_filtering.datePicker', handleDates)
		.on('comp_ui_filtering.resetFilters', resetFilters)
		.on('comp_ui_paging.pageChanged', pagingHandler);

	function handleInputs(e, a) {
		if (a.value !== '' && a.value !== 'all') {
				filtersData[a.type] = a.value;
			} else {
				delete filtersData[a.type];
			}
			filtersData.returnOffset = 0;
			$els.trigger('filters_updateTable', filtersData);
	}

	function handleDates(e, dates) {
		for (var o in dates) {
					if (dates.hasOwnProperty(o) && dates[o]) {
						filtersData[o] = dates[o];
					} else {
						delete filtersData[o];
					}
				}
			filtersData.returnOffset = 0;
			$els.trigger('filters_updateTable', filtersData);
	}

	function pagingHandler(e, page) {
		filtersData.returnOffset = (page.p2-1)*opts.itemsToShow;
		$els.trigger('filters_updateTable', filtersData);
	}

	function resetFilters() {
		filtersData = $.extend({}, {orderbycolumnname: opts.defaultSortType, orderbydirection: opts.orderbydirection, returnCount: opts.itemsToShow, returnOffset: 0}, opts.extraFilterOpts);
		tableOptions.updating = false;
		$els.trigger('filters_updateTable', [filtersData, tableOptions]);
	}

	function render(d) {
		utils.getTemplate(opts.template, function(tmpl) {
				$els.html(tmpl(d)).hide().fadeIn('slow');
		});
	}

	return {
		render: render
	};
}

module.exports = modFiltering;
