'use strict';

function jumboTron($els, utils, options) {
	var defaults = {
		template: '/assets/templates/views/modhub/jumbo.hbs',
		game: ''
	};
	var opts = $.extend({}, defaults, options);
	render(opts);

	function render(d) {
		utils.getTemplate(opts.template, function(tmpl) {
			$els.html(tmpl(d)).hide().fadeIn('slow');
		});
	}
	return {
		render: render
	};
}
module.exports = jumboTron;
