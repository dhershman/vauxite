'use strict';

function modListView($els, utils, models, options) {
	var defaults = {
		template: '/assets/templates/views/modhub/modlist.hbs',
		game: ''
	};
	var opts = $.extend({}, defaults, options);
	getMods();

	function getMods() {
		models.getMods.fetch({
				game: opts.game
			})
			.done(render)
			.fail(function throwErr() {
				throw new Error('Error on obtaining mods');
			});

	}

	function render(d) {
		var context = {
			items: d,
			game: opts.game
		};
		utils.getTemplate(opts.template, function(tmpl) {
			$els.html(tmpl(context)).hide().fadeIn('slow');
		});
	}

	return {
		getMods: getMods,
		render: render
	};
}

module.exports = modListView;
