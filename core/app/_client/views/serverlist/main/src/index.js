'use strict';

function serverListView($els, utils, options) {
	var defaults = {
		template: '/assets/templates/views/serverlist/server-list.hbs'
	};
	var opts = $.extend({}, defaults, options);
	render();

	function render(d) {
		utils.getTemplate(opts.template, function(tmpl) {
			$els.html(tmpl(d)).fadeIn('slow');
		});
	}
	return {
		render: render
	};
}

module.exports = serverListView;
