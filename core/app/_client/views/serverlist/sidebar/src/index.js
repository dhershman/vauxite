'use strict';

function sidebarView($els, utils, options) {
	var defaults = {
		template: '/assets/templates/views/serverlist/sidebar.hbs'
	};
	var opts = $.extend({}, defaults, options);
	render();

	function render(d) {
		utils.getTemplate(opts.template, function(tmpl) {
			$els.html(tmpl(d)).fadeIn('slow');
		});
	}
	return {
		render: render
	};
}

module.exports = sidebarView;
