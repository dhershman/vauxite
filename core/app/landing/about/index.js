'use strict';

module.exports = (request, reply) => {
	reply.view('landing/about/index', {
		page: {
			title: 'Vauxite - About',
			isAuthed: request.auth.isAuthenticated,
			user: (request.auth.isAuthenticated) ?  request.auth.credentials.id.displayName : null,
			avatar: (request.auth.isAuthenticated) ? request.auth.credentials.id.avatar : null,
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: false
			},
			{
				crumb: 'About',
				link: '/about',
				active: true
			}]
		},
		js: {
			page: 'build/js/landing.about.bundle.js',
			vendor: ['jquery/dist/jquery.min.js', 'bootstrap/dist/js/bootstrap.min.js']
		},
		css: {
			page: 'build/css/landing/about/index.css'
		}
	});
};
