'use strict';

//utils
var getTemplate = require('../../../../sky-modules/handles');

//models
var findAccount = require('../../_client/models/accounts/findAccount');
var updateAccount = require('../../_client/models/accounts/updateAccount');

//views
var titleBarView = require('../../_client/views/account/title-bar');
var infoColsView = require('../../_client/views/account/info-columns');

/*
TODO: All of the data should now be set on account creation to a users account. We obtain that information from the findAccount Model
The controller should use this model and send the data where it needs it for the account page, the titlebar has its own view
so the 2nd row needs it's own view as well to populate using the account information

we will need to properly set mod data when a mod is submitted in order to nicely display them on the account page. This has not been implemented yet.

FOCUS: Finish breaking out the 2nd row view and get it rendering. Then we can move on to mod handling again.
 */


$(function() {

	function accountController() {
		var $page = $('#account_Wrapper');
		var models = {
			findAccount: findAccount(),
			updateAccount: updateAccount()
		};

		$page.on('click', '#modifyAcc', modifyAccount)
		.on('click', '#cancelAcc', cancelAccount)
		.on('click', '#updateAcc', saveAccount)
		.on('click', '#cancelUpd', getAccount);

		var titleBar;
		var infoCols = infoColsView($page.find('#infoCols'), getTemplate);
		var userData = {};
		getAccount();

		function getAccount() {
			titleBar = titleBarView($page.find('#titleBar'), getTemplate, {template: '/assets/templates/views/account/title-bar.hbs'});
			models.findAccount.fetch()
			.fail(function(err) {
				throw err;
			})
			.done(function(d) {
				userData = d;
				titleBar.render(d);
				infoCols.render(d);
			});
		}

		function modifyAccount() {
			titleBar = titleBarView($page.find('#titleBar'), getTemplate, {template: '/assets/templates/views/account/modify-info.hbs'});
			titleBar.render(userData);
			$page.find('#infoCols').empty();
		}

		function saveAccount() {

			var $form = $page.find('form');
			$form = $form.find('input, select');
			var data = {};
			for (var i = 0; i < $form.length; i++) {
				if ($form.eq(i).attr('name') === 'github' || $form.eq(i).attr('name') === 'twitter' || $form.eq(i).attr('name') === 'skype') {
					if (!data.social || !data.social.length) {
						data.social = [];
					}
					if ($form.eq(i).val() === '') continue;
					data.social.push(buildSocial($form.eq(i).attr('name'), $form.eq(i).val()));
				} else {
					data[$form.eq(i).attr('name')] = $form.eq(i).val();
				}
			}
			models.updateAccount.fetch(data).done(getAccount).fail(function(err) {
				throw err;
			});
		}

		function buildSocial(plat, d) {
			var socialData = {};
			switch(plat) {
				case 'github':
					socialData = {
						socialLink: 'https://github.com/' + d,
						socialName: 'GitHub',
						socialUser: d
					};
					break;
				case 'twitter':
					socialData = {
						socialLink: 'https://twitter.com/' + d,
						socialName: 'Twitter',
						socialUser: d
					};
					break;
				case 'skype':
					socialData = {
						socialLink: 'https://skype.com/' + d,
						socialName: 'Skype',
						socialUser: d
					};
					break;
				default:
					break;
			}
			return socialData;
		}

		function cancelAccount() {

		}
	}

	accountController();
});
