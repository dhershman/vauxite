'use strict';

module.exports = (request, reply) => {
	reply.view('landing/account/index', {
		page: {
			title: 'Vauxite - Account',
			isAuthed: request.auth.isAuthenticated,
			user: request.auth.credentials.id.displayName,
			avatar: (request.auth.isAuthenticated) ? request.auth.credentials.id.avatar : null,
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: true
			}]
		},
		js: {
			page: 'js/landing.account.bundle.js',
			vendor: ['jquery.min.js', 'bootstrap.min.js']
		},
		css: {
			page: 'css/landing/account/index.css'
		}
	});
};
