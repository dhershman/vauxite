'use strict';

//Page Views
var homeJumbo = require('../../_client/views/home/home-jumbo');
var homeSideBar = require('../../_client/views/home/home-sidebar');
var homeMain = require('../../_client/views/home/home-main');

//Models
var steamNews = require('../../_client/models/steam-news');

//Page utils
var getTemplate = require('../../../../sky-modules/handles');

$(function() {

	function homeLanding() {
		var $page = $('.js-home-page');
		var models = {
			steamNews: steamNews()
		};
		var sideBar = homeSideBar($page.find('.js-sidebar'), getTemplate);
		var mainView = homeMain($page.find('.js-main'), models.steamNews, getTemplate);
		var jumboView = homeJumbo($page.find('.js-jumbo'), getTemplate);
		mainView.render();
		sideBar.render();
		jumboView.render();
		$page.on('click', '.js-select-game', $.proxy(homeJumbo.setGame, homeJumbo));
	}
	homeLanding();
});
