'use strict';

module.exports = (request, reply) => {
	reply.view('landing/home/index', {
		page: {
			title: 'Vauxite - Main',
			isAuthed: request.auth.isAuthenticated,
			user: (request.auth.isAuthenticated) ?  request.auth.credentials.id.displayName : null,
			avatar: (request.auth.isAuthenticated) ? request.auth.credentials.id.avatar : null,
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: true
			}]
		},
		js: {
			page: 'js/landing.home.bundle.js',
			vendor: ['jquery.min.js', 'bootstrap.min.js']
		},
		css: {
			page: 'css/landing/home/index.css'
		}
	});
};
