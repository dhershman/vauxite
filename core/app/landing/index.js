'use strict';

exports.register = (server, options, next) => {

	server.route({
		path: '/account',
		method: 'GET',
		handler: (request, reply) => {
			return require('./account')(request, reply);
		}
	});

	server.route({
		path: '/steam/newsAPI',
		method: 'GET',
		config: {
			auth: false
		},
		handler: (request, reply) => {
			return reply.proxy({host: 'api.steampowered.com/ISteamNews/GetNewsForApp/v0002/' + request.url.search, port: 80, protocol: 'http'});
		}
	});

	server.route({
		path: '/',
		method: 'GET',
		config: {
			auth: {
				mode: 'optional'
			}
		},
		handler: (request, reply) => {
			return require('./home')(request, reply);
		}

	});

	server.route({
		path: '/about',
		method: 'GET',
		handler: (request, reply) => {
			return require('./about')(request, reply);
		},
		config: {
			auth: {
				mode: 'optional'
			}
		}
	});

	next();

};

exports.register.attributes = {
	pkg: require('./package.json')
};
