'use strict';


const AC = require('../../../sky-modules/air-caste');
const GK = require('../../../sky-modules/gate-keeper');
exports.register = (server, options, next) => {

	server.route({
		path: '/login',
		method: 'POST',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			let keeper = GK(request, reply);
			keeper.manualLogin(request.payload.login_username, request.payload.login_password);

		}
	});

	server.route({
		path: '/logout',
		method: 'GET',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			return reply().unstate('token').redirect('/');
		}
	});

	server.route({
		path: '/loginpage',
		method: 'GET',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			return require('./loginpage')(request, reply);
		}
	});

	server.route({
		path: '/register',
		method: 'POST',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			let keeper = GK(request, reply);
			keeper.addNewAccount({
				user: request.payload.user,
				email: request.payload.email,
				pass: request.payload.pass
			});
		}
	});

	server.route({
		path: '/lost-password',
		method: 'POST',
		config: {
			auth: false
		},
		handler: function(request, reply) {
			let keeper = GK(request, reply);
			// look up the user's account via their email //
			keeper.getAccountByEmail(request.payload.userEmail, function(o) {
				if (o) {
					AC.dispatchResetPasswordLink(o, function(e, m) {
						// this callback takes a moment to return //
						// TODO add an ajax loader to give user feedback //
						if (!e) {
							console.log(m);
						} else {
							for (var k in e) console.log('ERROR : ', k, e[k]);
							reply(new Error('Failed to recover email'));
						}
					});
				}
			});
		}
	});

	server.route({
		path: '/reset-password',
		method: 'POST',
		handler: function(request, reply) {
			let keeper = GK(request, reply);
			var nPass = request.body['pass'];
			// retrieve the user's email from the session to lookup their account and reset password //
			var email = request.vauxite_user.reset.email;
			// destory the session immediately after retrieving the stored email //
			request.vauxite_user.destroy();
			keeper.updatePassword(email, nPass, function(e, o) {
				if (o) {
					reply.status(200).send('ok');
				} else {
					reply.status(400).send('unable to update password');
				}
			});
		}
	});
	next();
};

exports.register.attributes = {
	pkg: require('./package.json')
};
