'use strict';

module.exports = (request, reply) => {
	reply.view('login/loginpage/index', {
		page: {
			title: 'Vauxite - Login',
			isAuthed: request.state.token,
			user: request.state.user,
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: false
			}, {
				crumb: 'Login',
				link: '/loginpage',
				active: true
			}]
		},
		js: {
			page: 'js/login.loginpage.bundle.js',
			vendor: ['jquery.min.js', 'bootstrap.min.js']
		}
	});
};
