'use strict';
//utils
var getTemplate = require('../../../../sky-modules/handles');
var getParamByName = require('../../_client/utils/getParameterByName');

//views
var modInfoView = require('../../_client/views/mod-details/mod-info');

//models
var getMods = require('../../_client/models/mods/get-mods');

$(function() {

	function modDetails() {
		var $page = $('#modDetails');

		var game = getParamByName('game');
		var mod = getParamByName('mod');

		var models = {
			getMods: getMods()
		};


		models.getMods.fetch({
			game: game,
			searchablename: mod
		})
		.done(function(res) {
			modInfoView($page.find('#modInfo'), getTemplate, {}, {mod: res});
		})
		.fail(function(err) {

		});

	}

	modDetails();

});
