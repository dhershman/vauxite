'use strict';

module.exports = (request, reply) => {
	reply.view('modhub/details/index', {
		page: {
			title: 'Vauxite - Mods',
			isAuthed: request.auth.isAuthenticated,
			user: (request.auth.isAuthenticated) ?  request.auth.credentials.id.displayName : null,
			avatar: (request.auth.isAuthenticated) ? request.auth.credentials.id.avatar : null,
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: false
			},
			{
				crumb: 'Mods',
				link: '/mods',
				active: false
			},
			{
				crumb: request.params.path,
				link: '/mods/details',
				active: true
			}]
		},
		js: {
			page: 'js/modhub.details.bundle.js',
			vendor: ['jquery.min.js', 'bootstrap.min.js']
		},
		css: {
			page: 'css/modhub/details/index.css'
		}
	});
};
