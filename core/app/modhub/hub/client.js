'use strict';

//utils
var getTemplate = require('../../../../sky-modules/handles');

//modules
var filters = require('../../../../sky-modules/filters');
// var paginator = require('modules/paginatify');

//Views
var modOptions = require('../../_client/views/modhub/options');
var modJumboView = require('../../_client/views/modhub/jumbo');
var modlistView = require('../../_client/views/modhub/modlists');
var modFilters = require('../../_client/views/modhub/filters');

//models
var getMods = require('../../_client/models/mods/get-mods');

$(function() {
	function modHub() {
		var jumboView, modList, modFiltersView;
		var $page = $('#mainModSection');
		var modOpts = modOptions($page.find('#gameOptions'), getTemplate);
		var filtering = new filters($page.find('#modListFilters'), {
				trackFilterHistory: true,
				$tableEls: $page.find('#gameModList')
			});
		var models = {
			getMods: getMods()
		};

		$page.on('click', '.js-gamebtn', itemSelect)
				.on('click', '.js-backBtn', resetOptions);

		function itemSelect(e) {
			var selected = $(e.currentTarget).data('type');
			$page.find('#gameOptions').fadeOut('slow', function() {
				jumboView = modJumboView($page.find('#gameJumbo'), getTemplate, {game: selected});
				modList = modlistView($page.find('#gameModList'), getTemplate, models, {game: selected});
				modFiltersView = modFilters($page.find('#modListFilters'), getTemplate, {
					filterHandler: filtering,
					game: selected,
					list: modList
				});
			});
		}

		function resetOptions() {
			var $els = $page.children();
				var i = 0, len = $els.length;

				for (i; i < len; i++) {
					$($els[i]).fadeOut('fast', function() {
						$(this).empty();
					});
				}
				modOpts.render();
		}
	}
	modHub();
});
