'use strict';

module.exports = (request, reply) => {
	reply.view('modhub/hub/index', {
		page: {
			title: 'Vauxite - Mods',
			isAuthed: request.auth.isAuthenticated,
			user: (request.auth.isAuthenticated) ?  request.auth.credentials.id.displayName : null,
			avatar: (request.auth.isAuthenticated) ? request.auth.credentials.id.avatar : null,
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: false
			},
			{
				crumb: 'Mods',
				link: '/mods',
				active: true
			}]
		},
		css: {
			page: 'css/modhub/hub/index.css'
		},
		js: {
			page: 'js/modhub.hub.bundle.js',
			vendor: ['jquery.min.js', 'bootstrap.min.js']
		}
	});
};
