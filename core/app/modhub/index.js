'use strict';

const fs = require('fs');

exports.register = (server, options, next) => {

	server.route({
		path: '/mods',
		method: 'GET',
		config: {
			auth: {
				mode: 'optional'
			}
		},
		handler: (request, reply) => {
			return require('./hub')(request, reply);
		}
	});

	server.route({
		path: '/mods/submission',
		method: 'GET',
		handler: (request, reply) => {
			return require('./submit')(request, reply);
		}
	});

	server.route({
		path: '/mods/details',
		method: 'GET',
		config: {
			auth: {
				mode: 'optional'
			}
		},
		handler: (request, reply) => {
			return require('./details')(request, reply);
		}
	});

	server.route({
		path: '/mods/upload',
		method: 'POST',
		config: {
			payload: {
				output: 'stream',
				parse: true,
				allow: 'multipart/form-data'
			}
		},
		handler: (request, reply) => {
			let data = request.payload;
			if (data.file) {
				let name = data.file.hapi.filename;
				let path = '../../public/uploads/' + name;
				let file = fs.createWriteSteam(path);

				file.on('error', (err) => {
					console.error(err);
				});
				data.file.pipe(file);
				data.file.on('end', (err) => {
					let ret = {
						filename: data.file.hapi.filename,
						headers: data.file.hapi.headers
					};
					reply(JSON.stringify(ret));
				});
			}
		}
	});

	next();

};

exports.register.attributes = {
	pkg: require('./package.json')
};
