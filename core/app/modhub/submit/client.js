'use strict';

//utils
var getTemplate = require('../../../../sky-modules/handles');
var getParamByName = require('../../_client/utils/getParameterByName');
var validationLib = require('../../_client/utils/validationLib');

//models
var modSubmission = require('../../_client/models/mods/submit-mod');
var fileUpload = require('../../_client/models/upload-file/upload-file');

$(function() {

	function submitMod() {
		var $page = $('#pluginSubmit');
		var models = {
			modSubmission: modSubmission(),
			fileUpload: fileUpload()
		};

		var game = getParamByName('game');
		var quill = new Quill('#editor', {
			theme: 'snow',
			modules: {
				'syntax': true,
				'toolbar': [
				[{size: []}, {font: []}],
				[{color: []}, {background: []}],
				[{align: []}, {list: 'ordered'}, {list: 'bullet'}, {indent: '-1'}, {indent: '+1'}],
				['bold', 'italic', 'underline', 'strike'],
				['link', 'image', 'video', 'blockquote', 'code-block']]
			}
		});

		$page.on('click', '#modSubmitBtn', submission)
			.on('click', '.upload-btn', function() {
				if (!validateSingle($('#modTitle').val(), $('#modTitle'))) return;
				$page.find('#upload-input').click();
				$page.find('.progress-bar').text('0%');
				$page.find('.progress-bar').width('0%');
			})
			.on('change', '#upload-input', uploadFile);

		function uploadFile(e) {
			var formData = new FormData();
			var files = $(e.currentTarget).get(0).files;
			if (files.length > 0) {
				formData.append('modFile', files[0]);
				formData.append('name', $page.find('#modTitle').val().replace(' ', '-'));
				models.fileUpload.fetch(formData)
				.done(function runDone(res) {
					if (res.error) {
						nameTaken();
					}
					console.log(res);
				})
				.fail(function runError(err) {
					console.log(err);
				});
			}
		}

		function submission(e) {
			var data = {};
			var $form = $page.find('input, textarea, select');
			var i = 0, len = $form.length;
			for (i; i < len; i++) {
				if ($($form[i]).data('type') === 'version' && !$($form[i]).val()) $($form[i]).val('1.0.0');
				if ($($form[i]).val()) data[$($form[i]).data('type')] = $($form[i]).val();
			}

			console.log($page.find('.ql-editor').html());
			data.description = $page.find('.ql-editor').html();
			data.tags = (data.tags) ? data.tags.replace(/\w/g, '').split(',') : [];
			data.dependencies = (data.dependencies) ? data.dependencies.replace(/\w/g, '').split(',') : [];
			data.opdependencies = (data.opdependencies) ? data.opdependencies.replace(/\w/g, '').split(',') : [];
			data.game = game;
			data.searchablename = data.name.replace(' ', '-');
			data.thumbnail = (data.thumbnail) ? data.thumbnail : '/assets/build/img/placeholders/' + Math.ceil(Math.random() * 33) + '.jpg';
			data.modpath = '/mods/details?game=' + data.game + '&mod=' + data.searchablename;
			sendMod(data);
		}

		function sendMod(d) {
			models.modSubmission.fetch(d)
			.done(function(res) {
				window.location.href = '/mods/details?game=' + d.game + '&mod=' + d.searchablename;
			})
			.fail(function(err) {
				console.log(err);
			});
		}

		function validateAll() {
			var $items = $page.find('input, textarea, select');
			var errorCount = 0;
			var i = 0;
			var len = $items.length;
			for (i; i < len; i++) {
				if ($($items[i]).attr('id') === 'upload-input' && $($items[i]).val()) continue;
				if (!validateSingle($($items[i]).val(), $($items[i]))) errorCount++;
			}
			return errorCount;
		}

		function nameTaken() {

		}

		function warningModal() {

		}

		function validateSingle(d, $el) {
			var validation = validationLib.isValid(d, $el.data());
			if (validation.isValid) {
				$el.siblings('#error').addClass('hidden');
				return true;
			} else {
				$el.siblings('#error').removeClass('hidden');
				return false;
			}
		}
	}
	submitMod();
});
