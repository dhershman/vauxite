'use strict';

module.exports = (request, reply) => {
	reply.view('modhub/submit/index', {
		page: {
			game: request.url.query.game,
			isAuthed: request.auth.isAuthenticated,
			user: (request.auth.isAuthenticated) ?  request.auth.credentials.id.displayName : null,
			avatar: (request.auth.isAuthenticated) ? request.auth.credentials.id.avatar : null,
			title: 'Vauxite - Submit Mod',
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: false
			},
			{
				crumb: 'Mods',
				link: '/mods',
				active: false
			},
			{
				crumb: 'Submit Mod',
				link: '/mods/submission',
				active: true
			}]
		},
		css: {
			cdn: ['//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.5.0/styles/default.min.css', 'https://cdn.quilljs.com/1.0.0-beta.9/quill.snow.css'],
			page: ['css/modhub/submit/index.css']
		},
		js: {
			page: 'js/modhub.submit.bundle.js',
			vendor: ['jquery.min.js', 'bootstrap.min.js'],
			cdn: ['//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.5.0/highlight.min.js','https://cdn.quilljs.com/1.0.0-beta.9/quill.js']
		}
	});
};
