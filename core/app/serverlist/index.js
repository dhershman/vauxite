'use strict';

exports.register = (server, options, next) => {

	server.route({
		path: '/serverlist/main',
		method: 'GET',
		config: {
			auth: {
				mode: 'optional'
			}
		},
		handler: (request, reply) => {
			return require('./main')(request, reply);
		}
	});

	next();

};

exports.register.attributes = {
	pkg: require('./package.json')
};
