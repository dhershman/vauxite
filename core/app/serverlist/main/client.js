'use strict';

var mainContent = require('../../_client/views/serverlist/main');
var sidebar = require('../../_client/views/serverlist/sidebar');

var getTemplate = require('../../../../sky-modules/handles');

$(function() {

	function serverListController() {
		var $page = $('#serverListMain');
		var sidebar = sidebar($page.find('#serverListSidebar'), getTemplate);
		var mainView = mainContent($page.find('#serverListContent'), getTemplate);
	}

	return serverListController();
});
