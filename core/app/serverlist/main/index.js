'use strict';

module.exports = (request, reply) => {
	reply.view('serverlist/main/index', {
		page: {
			title: 'Vauxite - Server List',
			isAuthed: request.auth.isAuthenticated,
			user: (request.auth.isAuthenticated) ?  request.auth.credentials.id.displayName : null,
			avatar: (request.auth.isAuthenticated) ? request.auth.credentials.id.avatar : null,
			breadcrumbs: [{
				crumb: 'Home',
				link: '/',
				active: false
			},
			{
				crumb: 'Server Lists',
				link: '/serverlist/main',
				active: true
			}]
		},
		js: {
			page: 'js/serverlist.main.bundle.js',
			vendor: ['jquery.min.js', 'bootstrap.min.js']
		},
		css: {
			page: 'css/landing/home/index.css'
		}
	});
};
