$(document).ready(function() {
	$('body').on('submit', '#register', function(e) {
		e.preventDefault();
    e.stopImmediatePropagation();
		var $inputs = $(e.currentTarget).find('input');
		var info = {};
    var errorCount = 0;
		for (var i = 0; i < $inputs.length; i++) {
			if($inputs.eq(i).attr('name')) {
        if ($inputs.eq(i).val() === '') {
          errorCount++;
          $inputs.eq(i).siblings('#error').removeClass('hidden')
          .children('#validUser').removeClass('hidden');
        } else {
          info[$inputs.eq(i).attr('name')] = $inputs.eq(i).val();
          $inputs.eq(i).siblings('#error').addClass('hidden').children('#validUser').addClass('hidden');
        }
			}
		}
    if (!errorCount) fetch(info);
	});
});

function fetch(args) {
  $.ajax({
    type: 'POST',
    async: true,
    url: '/register',
    data: args,
    success: function() {
      window.location.href = '/';
    },
    error: handleError
  });
}

function handleError(err) {
  var $form = $('body').find('#register');
  var $item;
  switch(err.responseJSON.message) {
    case 'username-taken':
      $item = $form.find('#username');
      $item.siblings('#error').removeClass('hidden');
      $item.siblings('#error')
      .children('#validUser').addClass('hidden')
      .children('#userTaken').removeClass('hidden');
    break;
    default:
    break;
  }
}
