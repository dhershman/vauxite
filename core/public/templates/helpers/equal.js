'use strict';

var Handlebars = require('handlebars');

module.exports = function() {
	Handlebars.registerHelper('equals', function(context, string, options) {
		if(context != null && context.toString().length && context.toLowerCase() === string.toLowerCase()) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	});
};