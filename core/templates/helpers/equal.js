'use strict';

var Handlebars = require('handlebars');

module.exports = function() {
	console.log('Register');
	Handlebars.registerHelper('equal', function(context, string, options) {
		if(context != null && context.toString().length && context === string) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	});
};