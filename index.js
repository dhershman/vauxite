'use strict';
const path = require('path');
const Hapi = require('hapi');
const server = new Hapi.Server();

server.connection({
	port: 5000
});


server.register([{
		register: require('hapi-rethinkdb'),
		options: {
			url: 'rethinkdb://localhost:28015/vauxite'
		}
	},
	require('./sky-modules/inquisitor'),
	require('./sky-modules/hapi-lantern'),
	require('h2o2'),
	require('vision'),
	require('inert'),
	require('./lib'),
	require('./core/app/landing'),
	require('./core/app/modhub'),
	require('./core/app/serverlist'),
	require('./core/app/login')
], (err) => {
	if (err) throw err;

	server.route({
		method: 'GET',
		path: '/assets/{param*}',
		config: {
			auth: false
		},
		handler: {
			directory: {
				path: path.join(__dirname, 'core/public')
			}
		}
	});

	server.views({
		engines: {
			hbs: require('handlebars')
		},
		path: './core/app',
		layoutPath: './core/templates/layouts',
		helpersPath: './core/templates/helpers',
		partialsPath: './core/templates/partials',
		layout: 'main'
	});

	server.start((err) => {
		if (err) throw err;
		console.log('Server running at: ' + server.info.uri);
		server.log('info', 'Server running at: ' + server.info.uri);
	});

});
