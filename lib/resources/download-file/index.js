'use strict';

const fs = require('fs');
const boom = require('boom');

module.exports = (request, reply) => {

	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	let payload = request.payload;
	r.table('mod_files').filter({filename: payload.searchablename}).pluck('file', 'filename').run(conn, (err, results) => {
		if (err) throw boom.badImplementation('Unable to read file');
		return reply(results);
	});
};
