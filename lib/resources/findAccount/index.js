'use strict';

const _ = require('lodash');

module.exports = (request, reply) => {
	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	r.table('accounts').filter({id: request.auth.credentials.id.id}).run(conn, (err, cursor) => {
		cursor.toArray((err, results) => {
			let data = _.pick(results[0], ['email', 'displayName', 'mods', 'id', 'title', 'location', 'gender', 'avatar', 'social', 'age', 'stats', 'payee']);
			return reply(data);
		});
	});
};