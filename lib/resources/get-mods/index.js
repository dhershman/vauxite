'use strict';

const joi = require('joi');
const boom = require('boom');


let schema = joi.object().keys({
	game: joi.string().required(),
	searchterm: joi.string().optional(),
	searchablename: joi.string().optional(),
	startdate: joi.string().optional(),
	enddate: joi.string().optional()
});

module.exports = (request, reply) => {

	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;


	let payload = request.query;
	let results = joi.validate(payload, schema);
	if (results.error) return reply(boom.badRequest('incorrect parameters in query: ' + results.error));

	if (payload.searchablename) {
		r.table(payload.game + '_modding').filter(r.row('searchablename').eq(payload.searchablename)).run(conn, (err, cursor) => {
			if (err) throw boom.badImplementation('Unable to obtain table');
			getResults(cursor);
		});
	} else {
		r.table(payload.game + '_modding').run(conn, (err, cursor) => {
			if (err) throw boom.badImplementation('Unable to obtain table');
			getResults(cursor);
		});
	}

	let getResults = (cursor) => {
		cursor.toArray((err, result) => {
			if (err) throw boom.badImplementation('Unable to obtain payload from table');
			return reply(JSON.stringify(result));
		});
	};
};
