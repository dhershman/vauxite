'use strict';

const boom = require('boom');
module.exports = (request, reply) => {

	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	console.log(request.payload);
	let payload = JSON.parse(request.payload);
	r.table(payload.game + '_modding').filter({searchablename: payload.mod}).run(conn, (err, cursor) => {
		if (err) throw boom.badImplementation('Unable to insert payload into database');
		cursor.toArray((err, results) => {
			if (results[0]) {
				return reply({error: true, message: 'Name Taken'});
			} else {
				r.table(payload.game + '_modding').insert(payload).run(conn, (err, response) => {
					r.table('accounts').get('mods').append(response.generated_keys).run(conn, (err, response) => {
						if (err) console.log(err);
						return reply(response);
					});
				});
			}
		});
	});
};
