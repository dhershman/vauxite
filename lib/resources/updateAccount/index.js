'use strict';

const boom = require('boom');

module.exports = (request, reply) => {
	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	let payload = JSON.parse(request.payload.info);
	r.table('accounts').filter({id: request.auth.credentials.id.id}).update(payload).run(conn, (err, results) => {
			return reply(results);
		});
	};