'use strict';

const fs = require('fs');
const boom = require('boom');

module.exports = (request, reply) => {

	let r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	let conn = request.server.plugins['hapi-rethinkdb'].connection;

	let payload = request.payload;
	console.log(payload.modFile._data);
	r.table('mod_files').insert({
		user: request.state.user,
		filename: payload.name,
		file: payload.modFile._data
	}).run(conn, (err, results) => {
		if (err) throw boom.badImplementation('Unable to read file');
		console.log(results);
		return reply(results);
	});
};
