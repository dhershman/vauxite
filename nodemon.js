const nodemon = require('nodemon');
const nmJSON = require('./nodemon.json');
const tasks = {
	build: require('./tasks/build.js'),
	compileSass: require('./tasks/compileSass.js'),
	minImgs: require('./tasks/minImages.js')
};

nodemon(nmJSON);

nodemon.on('restart', function(files) {
	runTask(files[0]);
	console.log('Restarted due to: ', files);
}).on('start', function() {
	console.log('Nodemon Done...');
}).on('exit', function() {
	console.log('App Shutting down.');
});

function runTask(file) {
	var type = file.substr(file.lastIndexOf('.') + 1);
	switch(type) {
		case 'js':
			tasks.build();
			break;
		case 'scss':
			tasks.compileSass();
			break;
		case 'png':
			tasks.minImgs();
			break;
		case 'jpg':
			tasks.minImgs();
			break;
		default:
			break;
	}
}