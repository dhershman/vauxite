## Info
FilterHandler is just as it's called, it plugs into a table, or wherever you need and can handle filters on the fly easily and effectively. Please note this is a successor to the FiltersBuilder module I was working on before. Due to the shift in direction I decided to consider it now depricated and create a handler more so than a builder/renderer.

## Features
- Event Based
- Easy plug and play classes
- Customizable
- Sends data back for custom database calls
- Built in datepicker handling
- Unique Features for better user experiences

## How-To
Setting up the module is simple enough.

```js
var filterhandler = require('filterhandler');
var myFilters = new filterhandler($(pageel), options);
```

##### Arguments
- `els` - `jQuery Object` the page element that the filters should be rendered on
- `options` - `Object` an object with options such as the filters setup you want.

##Options
- `autoSearchWait` - `Number` how long should auto search wait after a user stops typing (this is in miliseconds). Default `1000`
- `allowHighlighting` - `Boolean` pass in true if you want to have filters highlight items that are searched for. Default: `false`
- `trackFilterHistory` - `Boolean` pass true if you want filters history to be saved. Default: `false`
- `preventEmptyFilters` - `Boolean` pass true if you do not want the filters to trigger if the box is empty. Default: `false`
- `dateFormat` - `String` passed to format the date to whatever you want (uses moment) Default: `MM/DD/YYYY`
- `$tableEls` - `jQuery Object` the element that the filters will be "Filtering" Default: `''`

##Events
`filterhandler.dropDown` - fired whenever a dropdown is changed it will send the event and an object with the data value of the dropdown

`filterhandler.input` - Fired when a radio button is chanded, and when a user hits enter or clicks off a regular input box

`filterhandler.savedHistory` - Fired whenever a save happens if `trackFilterHistory` is `true`

`filterhandler.datePicker` - fired when the datepicker is successfully entered

`filterhandler.resetFilters` - fired when the reset filters link/button is clicked

`filterhandler.checkbox` - triggers with checkbox manipulation

`apply.daterangepicker` - a default event fired by daterangepicker whenever someone applies a new date

## Classes
#### Classes with `js-` in the front are not style based classes and are used by the javascript exclusively. 
#### All bootstrap elements are also supported such as form-control etc. (if bootstrap is present)

###Available Classes
*All classes are explained below this section*

- [js-filterdropdown](#js-filterdropdown)
- [js-filterinput](#js-filterinput)
- [js-filter-checkbox](#js-filter-checkbox)
- [js-clickable-search](#js-clickable-search)
- [js-datepicker](#js-datepicker)
- [js-daterange](#js-daterange)
- [js-datepicker-icon](#js-datepicker-icon)
- [js-resetfilters](#js-resetfilters)
- [js-watchhighlight](#js-watchhighlight)
- [js-savehistory](#js-savehistory)

###js-filterdropdown
Should be attached to the drop down so it can properly assign data and trigger the proper events

```html
<select data-type="reason" class="js-filterDropdown">
        <option value="reason1">No Sale</option>
        <option value="reason2">Return</option>
</select>
```

#### Usage
Triggers the `filterhandler.dropDown` event when the element is `changed` this can also be used with regular `inputs` not just `dropdowns`

```js
$el.on('filterhandler.input', $.proxy(this.handleInputs, this)).on('filterhandler.dropDown', $.proxy(this.handleInputs, this));
function handleInputs(e, a) {
    var filtersData = {};
    if (a.value !== '' && a.value !== 'all') {
        filtersData[a.type] = a.value;
    } else {
        delete filtersData[a.type];
    }
    return filtersData;
};
```

###js-filterinput
Attach to inputs such as `text`, `number`, and `radio button` inputs. 

returns an object like so:
```js
{
    value: 'test',
    type: 'searchterm'
}
```

```html
<input class="js-filterInput" type="text" value="" data-type="searchterm" placeholder="All">
```

###js-filter-checkbox
Attach to checkbox inputs to handle them properly returns an object like so: 
```js
{
    value: true,
    type: 'searchtype'
}
```

```html
<input class="js-filter-checkbox" type="checkbox"  data-type="searchtype">
```

###js-datepicker
Should be attached to a input you would like to use as a date picker element this will attach the bootstrap date picker to this element and allow the proper events to trigger on it.

```html
<input type="text" class="form-control js-datepicker">
```

#### Usage
Triggers the `filterhandler.datePicker` event when the user hits the enter key, or clicks on a certain date through the Calendar. 
Dates return as an object:  
```js
{
    startdate: 'XX/XX/XXXX',
    enddate: 'XX/XX/XXXX'
}
```

```js
$el.on('filterhandler.datePicker', $.proxy(this.handleDates, this));
 function handleDates(e, dates) {
    var filtersData = {};
        for (var o in dates) {
            if (dates.hasOwnProperty(o) && dates[o]) {
                filtersData[o] = dates[o];
            } else {
                delete filtersData[o];
            }
        }
    return filtersData;
};
```

###js-clickable-search
This class will allow a user to click on an item in your table or whatever element filterhandler is watching in order to automatically make a search request for that data-type and its value.

```html
<span data-type="searchterm" class="js-clickable-search"></span>
```

###js-daterange
Attach this class to the wrapper div of your inputs if you plan to use the date range picker in order to correctly plan out date range requires the use of `datepickerFrom` and `datepickerTo` in order to properly place the correct values.

```html
<div class="input-group input-daterange js-daterange">
    <input type="text" class="form-control js-datepicker" id="datepickerFrom">
    <span class="input-group-addon">to</span>
    <input type="text" class="form-control js-datepicker" id="datepickerTo">
</div>
```

#### Usage
Used to keep track of date ranges so that users cannot mis match ranges
still triggers `filterhandler.datePicker` once these calculations finish

###js-datepicker-icon
Attach this if you have icons on your datepicker inputs and would like the icon to also trigger a date picker calendar to appear on your input.

```html
<span class="input-group-addon js-datepicker-icon"><span class="glyphicon glyphicon-calendar"></span></span>
```

Use it properly with the date picker element would look like:
```html
<div class="input-group">
    <input type="text" class="form-control js-datepicker">
    <span class="input-group-addon js-datepicker-icon"><span class="glyphicon glyphicon-calendar"></span></span>
</div>
```

###js-resetfilters
Attach this on the button or link you would like to use to trigger a filter reset

```html
<a class="js-resetFilters">Reset Filters</a>
```

#### Usage
Triggers the `filterhandler.resetFilters` event but does not pass along any data with the event
```js
$el.on('filterhandler.resetFilters', $.proxy(this.resetFilters, this));
 function resetFilters() {
        this.filtersData = {};
        return this.filtersData;
    };
```

###js-watchhighlight
Attach this class on to elements you would like to be highlighted when the user uses a search input, if `allowHighlighting` is set to true then items with this class will trigger a highlight within your table or whatever you are using for highlights. 

######Important
This adds a class called `comp--mark` to items that are found by the highlighting function so make sure you setup in your css for this class or you will not see your highlights.

```html
<span class="js-watchhighlight"></span>
```

#### Usage
In order to actually trigger highlight you will need to call the function after your table, or whatever else you are filtering re-renders. Sending the function the term you want highlighted.

```js
function render(opts) {
   Filters.highlightItems(opts.searchterm);
}
```

###js-savehistory
Attach this class to elements you would like Filtering to save in history (the value not the element).

```html
<input type="text" class="form-control js-datepicker js-savehistory">
```

#### Usage
In order to access the filters history simply make a request to the history object inside filters
```js
yourview.filters.history
```

## Data Types
Data types play a pretty important role in getting you the right information for your use. 

###type 
type is the basic data type of the bunch it tells you what the type of input your data is coming from, useful if you want a filter to call a sql service when a user hits enter. (the return will give you back the type and the value of the input in an object with the trigger)

####Usage
```html
<input class="form-control js-filterinput js-savehistory" type="text" value="Example" data-type="searchterm" placeholder="All">
```
This would return the following object to your listener:
```js
{value: 'Example', type: 'searchterm'}
```
