/**
 *  @author: Dustin Hershman
 *  @version 0.1.0
 *  @since 01/12/2016
 *  @summary handler for filtering systems
 **/
	var debounce = require('dynamic-timeouts');
	var daterangepicker = require('bootstrap-daterangepicker');
	var Filtering = function(els, options) {
		this.$elements = els;
		this.defaults = {
			dateFormat: 'MM/DD/YYYY',
			trackFilterHistory: false,
			allowHighlighting: false,
			preventEmptyFilters: false,
			autoSearchWait: 1000,
			$tableEls: ''
		};
		this.opts = $.extend({}, this.defaults, options);
		this.history = {};
		this.events();
	};

	Filtering.prototype.events = function() {
		var _this = this;

		this.$elements.on('change', '.js-filterdropdown', function() {
			_this.$elements.trigger('filterhandler.dropDown', {value: $(this).val(), type: $(this).data('type')});
		})
		.on('change', '.js-filter-checkbox', $.proxy(this.handleCheckbox, this))
		.on('keyup', '.js-datepicker', $.proxy(this.setDate, this))
		.on('click', '.js-datepicker-icon', function() {
			_this.$elements.find('.js-datepicker').focus();
		})
		.on('focus', '.js-datepicker', $.proxy(this.showCalendar, this))
		.on('apply.daterangepicker', '.js-datepicker', $.proxy(this.setDate, this))
		.on('click', '.js-resetfilters', $.proxy(this.clearFilters, this))
		.on('keypress', '.js-filterinput', $.proxy(debounce(this.handleSearch, this.opts.autoSearchWait), this))
		.on('blur', '.js-filterinput', $.proxy(this.handleSearch, this));
		this.opts.$tableEls.on('click', '.js-clickable-search', $.proxy(this.handleClickSearch, this));
	};

	Filtering.prototype.handleSearch = function(e) {
		var search = e.currentTarget;
		if ($(search).val() === this.history[$(search).data('type')] || this.opts.preventEmptyFilters && $(search).val() === '') return;
		this.$elements.trigger('filterhandler.input', {value: $(search).val(), type: $(search).data('type') || 'NOTSET'});
		if (this.opts.trackFilterHistory) {
			this.history[$(search).data('type')] = $(search).val();
			this.$elements.trigger('filterhandler.savedHistory', this.history);
		}
	};

	Filtering.prototype.handleClickSearch = function(e) {
		var search = e.currentTarget;
		if ($(search).text() === this.history[$(search).data('type')] || this.opts.preventEmptyFilters && $(search).text() === '') return;
		this.$elements.trigger('filterhandler.input', {value: $(search).text(), type: $(search).data('type') || 'NOTSET'});
		if (this.opts.trackFilterHistory) {
			this.history[$(search).data('type')] = $(search).text();
			this.$elements.trigger('filterhandler.savedHistory', this.history);
		}
	};

	Filtering.prototype.handleCheckbox = function(e) {
		var search = e.currentTarget;
		this.$elements.trigger('filterhandler.checkbox', {value: $(search).is(':checked'), type: $(search).data('type') || 'NOTSET'});
		if (this.opts.trackFilterHistory) {
			this.history[$(search).data('type')] = $(search).is(':checked');
			this.$elements.trigger('filterhandler.savedHistory', this.history);
		}
	};

	Filtering.prototype.showCalendar = function(e) {
		$(e.currentTarget).val('');
		var dateObj = {
			startdate: this.$elements.find('#datepickerFrom').val() || Infinity,
			enddate: this.$elements.find('#datepickerTo').val() || Infinity
		};
		$(e.currentTarget).daterangepicker({
			autoApply: true,
			autoUpdateInput: false,
			singleDatePicker: true,
			minDate: dateObj.startdate,
			maxDate: dateObj.enddate
		});
	};

	Filtering.prototype.setDate = function(e, picker) {
		if (this.opts.preventEmptyFilters && $(e.currentTarget).val() === '') return;
		if (e.type === 'keyup') {
			if (e.keyCode !== 13) {
				return;
			} else {
				this.$elements.trigger('filterhandler.datePicker', {
					startdate: this.$elements.find('#datepickerFrom').val() || picker.startDate.format(this.opts.dateFormat),
					enddate: this.$elements.find('#datepickerTo').val() || picker.endDate.format(this.opts.dateFormat)
				});
			}
		} else {
			$(e.currentTarget).val(picker.startDate.format(this.opts.dateFormat));
			if (this.$elements.find('.js-daterange').length > 0
				&& this.$elements.find('#datepickerFrom').val() !== ''
				&& this.$elements.find('#datepickerTo').val() !== '') {
				this.$elements.trigger('filterhandler.datePicker', {
					startdate: this.$elements.find('#datepickerFrom').val(),
					enddate: this.$elements.find('#datepickerTo').val()
				});

			} else {
				this.$elements.trigger('filterhandler.datePicker', {
					startdate: picker.startDate.format(this.opts.dateFormat),
					enddate: picker.endDate.format(this.opts.dateFormat)
				});
			}
		}
		if (this.opts.trackFilterHistory) {
			this.history.startdate = this.$elements.find('#datepickerFrom').val() || picker.startDate.format(this.opts.dateFormat);
			this.history.enddate = this.$elements.find('#datepickerTo').val() || picker.endDate.format(this.opts.dateFormat);
			this.$elements.trigger('filterhandler.savedHistory', this.history);
		}
	};

	Filtering.prototype.clearFilters = function() {
		var inputs = this.$elements.find('input'), i = 0;
		var mainArr = inputs.add(this.$elements.find('select')), len = mainArr.length, item;
		for (i; i < len; i++) {
			item = mainArr[i];
			if ($(item).hasClass('js-filterinput') || $(item).hasClass('js-datepicker')) {
				$(item).val('');
			} else {
				$(item).prop('selectedIndex', 0);
			}
		}
		this.history = {};
		this.$elements.trigger('filterhandler.resetFilters').trigger('filterhandler.savedHistory', this.history);
		return false;
	};

	Filtering.prototype.highlightItems = function(term) {
		this.opts.$tableEls.find('.js-watchhighlight').filter(function() {
			if ($(this).text() === term) {
				$(this).addClass('filter--mark');
			}
		});
	};

	module.exports = Filtering;