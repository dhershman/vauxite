'use strict';

const crypto = require('crypto');
const moment = require('moment');
const Boom = require('boom');
const jwt = require('jsonwebtoken');

const cookie_options = {
  ttl: 1 * 24 * 60 * 60 * 1000, // expires a day from today
  encoding: 'none',    // we already used JWT to encode
  isSecure: false,      // warm & fuzzy feelings
  isHttpOnly: false,    // prevent client alteration
  clearInvalid: false, // remove invalid cookies
  strictHeader: true   // don't allow violations of RFC 6265
};

let conn, accounts, r;

/* login validation methods */

function gateKeeper(request, reply) {
	r = request.server.plugins['hapi-rethinkdb'].rethinkdb;
	conn = request.server.plugins['hapi-rethinkdb'].connection;
	accounts = r.table('accounts').run(conn); //Grabbing the accounts table cursor

	function autoLogin(user, pass) {
		r.table('accounts').filter(r.row('user').eq(user.toLowerCase()).limit(1)).run(conn, (err, cursor) => {
			if (err) throw Boom.badImplementation('Unable to connect table');
			cursor.toArray((err, result) => {
				if (err) throw Boom.badImplementation('Unable to find user');
				if (result[0] && result[0].pass === pass) {
					let token = createToken(result[0].id);
					reply({ token: token })
					.header('Authorization', token)
					.state('token', token, cookie_options)
					.redirect('/');
				} else {
					reply(null);
				}
			});
		});
	}

	function manualLogin(user, pass) {
		r.table('accounts').filter(r.row('user').eq(user.toLowerCase())).limit(1).run(conn, (err, cursor) => {
			if (err) throw Boom.badImplementation('Unable to connect table');
			cursor.toArray((err, result) => {
				if (err) throw Boom.badImplementation('Unable to connect user');

				if (result[0] == null) {
					reply('user-not-found');
				} else {
					validatePassword(pass, result[0].pass, function(err, res) {
						let token = createToken(result[0]);
						if (res) {
							if (request.payload.remember_me) {
								reply.state('pass', result[0].pass, {
									ttl: 86400000,
									encoding: 'base64json',
									parse: true
								});
							}
							if (request.query.redirect) {
								return reply({ token: token })
									.header('Authorization', token)
									.state('token', token, cookie_options)
									.redirect(request.query.redirect);
							} else {
								return reply({ token: token })
									.header('Authorization', token)
									.state('token', token, cookie_options)
									.redirect('/');
							}
						} else {
							reply('invalid-password');
						}
					});
				}
			});
		});
	}

	function addNewAccount(newData) {
		r.table('accounts').filter(r.row('user').eq(newData.user.toLowerCase())).run(conn, (err, cursor) => {
			if (err) throw Boom.badImplementation('Unable to connect table');
			cursor.toArray((err, result) => {
				if (err) throw Boom.badImplementation('Unable to connect user');
				if (result[0]) {
					throw Boom.conflict('username-taken', {type: 'username'});
				} else {
					if (!newData.pass) throw Boom.badData('password', {type:  'password'});
					if (!newData.user) throw Boom.badData('username', {type: 'username'});
					let oPass = newData.pass;
						newData.displayName = newData.user;
						newData.user = newData.user.toLowerCase();
					saltAndHash(newData.pass, (hash) => {
						newData.pass = hash;
						newData.admin = false;
						newData.mods = [];
						newData.social = [];
						newData.stats = {
							likes: 0,
							resCount: 0,
							pixelPoints: 0,
							lastLogin: moment().format('MMMM Do, YYYY'),
							dateJoined: moment().format('MMMM Do, YYYY')
						};
						newData.age = 0;
						newData.avatar = '';
						newData.title = 'Newbie';
						newData.gender = 'N/A';
						newData.location = 'N/A';
						newData.payee = {
							balance: 0,
							accStatus: 'Normal',
							renewDate: 'N/A'
						};
						r.table('accounts').insert(newData).run(conn, (err, result) => {
							let token = createToken(result.generated_keys[0]);
							manualLogin(newData.user, oPass);
						});
					});
				}
			});
		});
	}

	function updateAccount(newData, callback) {
		r.table('accounts').filter(r.row('user').eq(newData.user)).limit(1).run(conn, (err, cursor) => {
			if (err) throw Boom.badImplementation('Unable to connect table');
			cursor.toArray((err, result) => {
				if (err) throw Boom.badImplementation('Unable to connect user');
				var o = result[0];
				o.name = newData.name;
				o.email = newData.email;
				if (newData.pass === '') {
					r.table('accounts').update(o).run(conn, (err) => {
						if (err) callback(err);
						else callback(null, o);
					});
				} else {
					saltAndHash(newData.pass, function(hash) {
						o.pass = hash;
						r.table('accounts').update(o).run((err) => {
							if (err) callback(err);
							else callback(null, o);
						});
					});
				}
			});
		});
	}

	function updatePassword(email, newPass, callback) {
		r.table('accounts').filter(r.row('email').eq(email).limit(1)).run(conn, (err, cursor) => {
			if (err) throw Boom.badImplementation('Unable to connect table');
			cursor.toArray((err, result) => {
				if (err) throw Boom.badImplementation('Unable to connect user');
				var o = result[0];
				saltAndHash(newPass, function(hash) {
					o.pass = hash;
					r.table('accounts').update(o).run(conn, callback);
				});
			});
		});
	}

	function deleteAccount(id, callback) {
		r.table('accounts').filter(r.row('_id').eq(id).limit(1)).delete().run(conn, (err, result) => {
			if (err) throw Boom.badImplementation('Unable to connect user');
			callback(result);
		});
	}

	function getAccountByEmail(email, callback) {
		r.table('accounts').filter(r.row('email').eq(email).limit(1)).run(conn, (err, result) => {
			if (err) throw Boom.badImplementation('Unable to connect user');
			callback(result);
		});
	}

	function validateResetLink(email, passHash, callback) {
		r.table('accounts').filter({ email: email, pass: passHash }).run(conn, (err, result) => {
			if (err) throw Boom.badImplementation('Unable to connect user');
			callback(result ? 'ok' : null);
		});
	}

	function getAllRecords(callback) {
		r.table('accounts').toArray((err, results) => {
			if (err) throw Boom.badImplementation('Unable to get records');
			else callback(null, results);
		});
	}

	function delAllRecords(callback) {
		r.table('accounts').delete().run(conn, callback);
	}

	function createToken(id) {
		let secretKey = process.env.JWT || 'y00y-00m3-m4k1-z00m-y00y-00m3-m4k1-z00m-';
		return jwt.sign({
			id: id
		}, secretKey, { expiresIn: '18h' });
	}

	return {
		autoLogin: autoLogin,
		manualLogin: manualLogin,
		addNewAccount: addNewAccount,
		updateAccount: updateAccount,
		updatePassword: updatePassword,
		deleteAccount: deleteAccount,
		getAccountByEmail: getAccountByEmail,
		validateResetLink: validateResetLink,
		getAllRecords: getAllRecords,
		delAllRecords: delAllRecords,
		createToken: createToken
	};

}

/* private encryption & validation methods */

var generateSalt = function() {
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
};

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
};

var saltAndHash = function(pass, callback) {
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
};

var validatePassword = function(plainPass, hashedPass, callback) {
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
	callback(null, hashedPass === validHash);
};

/* auxiliary methods */

var getObjectId = function(id) {
	return new require('mongodb').ObjectID(id);
};

var findById = function(id, callback) {
	accounts.findOne({
			_id: getObjectId(id)
		},
		function(e, res) {
			if (e) callback(e);
			else callback(null, res);
		});
};


var findByMultipleFields = function(a, callback) {
	// this takes an array of name/val pairs to search against {fieldName : 'value'} //
	accounts.find({
		$or: a
	}).toArray(
		function(e, results) {
			if (e) callback(e);
			else callback(null, results);
		});
};

module.exports = gateKeeper;
