'use strict';
const Handlebars = require('handlebars');

if (!global.utils) global.utils = {};


exports.getTemplate = function(path, done) {
	if (global.utils.templateCache == null) {
		global.utils.templateCache = {};
	}
	if (global.utils.templateCache[path] != null) {
		done(global.utils.templateCache[path]);
	} else {
		$.get(path, function(contents) {
			global.utils.templateCache[path] = Handlebars.compile(contents);
			done(global.utils.templateCache[path]);
		});
	}
};

Handlebars.registerHelper('equal', function(context, string, options) {
	if (context != null && context.toString().length && context.toLowerCase() === string.toLowerCase()) {
		return options.fn(this);
	} else {
		return options.inverse(this);
	}
});

Handlebars.registerHelper('findSocial', function(context, string) {
	for (var i =0; i < context.length; i++) {
		if (context[i].socialName.toLowerCase() === string.toLowerCase()) {
			return context[i].socialUser;
		}
	}
});
