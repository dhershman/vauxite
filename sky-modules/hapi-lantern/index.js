'use strict';

exports.register = (server, options, next) => {
	server.ext('onPreResponse', (request, reply) => {

		if(request.response.isBoom) {
			let path ='';
			switch(request.response.output.statusCode) {
				case 401: //Unauthorized
					path += request.path;
					if (request.url.query.game) path += '?game=' + request.url.query.game;
					return reply().redirect('/loginpage?redirect="'+  path +'"');
				case 500: //Bad Request
					break;
				case 404: //Not Found
					break;
				case 422: //Bad Data
					break;
				case 409: //Conflict
					break;
				default:
					break;
			}
		}
		reply.continue();
	});

	next();
};

exports.register.attributes = {
	name: 'hapi-lantern'
};
