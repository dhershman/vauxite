'use strict';

// Declare internals

const jwt = require('hapi-auth-jwt2');

let r, conn;
exports.register = (server, options, next) => {
	r = server.plugins['hapi-rethinkdb'].rethinkdb;
	conn = server.plugins['hapi-rethinkdb'].connection;

	server.register(jwt, registerAuth);
	function registerAuth(err) {
		if (err) {
			return next(err);
		}

		server.auth.strategy('jwt', 'jwt',
		{
			key: process.env.JWT || 'y00y-00m3-m4k1-z00m-y00y-00m3-m4k1-z00m-',
			validateFunc: function(decoded, request, cb) {
				r.table('accounts').filter(r.row('id').eq(decoded.id.id)).run(conn, (err, cursor) => {
					cursor.toArray((err, results) => {
						if (!results.length) {
							return cb(null, false);
						}
						return cb(null, true);
					});
				});
			},
			verifyOptions: { algorithms: ['HS256'] }
		});
		server.auth.default('jwt');
		return next();
	}
};

exports.register.attributes = {
	name: 'inquisitor'
};

