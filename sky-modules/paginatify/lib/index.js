'use strict';

const $ = require('jquery');
const _ = require('lodash');

class paginatify {
    constructor(options) {
        this.defaults = {
        	$elements: null,
            currentPage: 1,
            itemsPerPage: 10,
            size: 'normal',
            alignment: 'center',
            text: {
                'first': 'First',
                'last': 'Last',
                'next': '&raquo;',
                'prev': '&laquo;',
                'page': null
            },
            btns: {
                'first': true,
                'last': true,
                'next': true,
                'prev': true
            }
        };
        if (this.defaults.$elements === null) return new Error('ERROR: No element set');
        this.opts = $.extend({}, this.defaults, options);
        this.init();
    }

    init() {

    }

    setPageTotal(a) {
    	this.pages = (Math.ceil(a/this.opts.itemsPerPage) === 0) ? 1 : Math.ceil(a/this.opts.itemsPerPage) ;
    }

    buildHTML() {
    	var i = 0;
    	var len = this.pages.length;
    	var html = '';
    	for (i; i < len; i++) {
    		html += '<'
    	}
    }

}