'use strict';

const browserify = require('browserify');
const uglify = require('uglify-stream');
const glob = require('glob');
const path = require('path');
const fs = require('fs');

let appPath = 'core/app/';
let assetPath = 'core/public/build/js';

function build() {
	glob(path.resolve(appPath, '**/client.js'), {}, (err, files) => {
		files.forEach((file) => {
			let entrySplit = file.substr(file.indexOf('app/'), file.indexOf('/client')).split('/');
			let fileName = entrySplit[1] + '.' + entrySplit[2];
			let bundleStream = fs.createWriteStream(assetPath + '/' + fileName + '.bundle.js');
			const b = browserify({debug: true});
			b.add(file);
			// b.plugin('minifyify', {map: false}); //Turn on once we want to push up to prod
			b.bundle()
				.pipe(bundleStream);
			bundleStream.on('finish', () => {
				console.log('finished bundling ' + fileName);
			});
		});
	});

	//Global JavaScripts and Custom things that need to exist all over
	glob(path.resolve('core/public', 'js/global/**.js'), {}, (err, files) => {
		files.forEach((file) => {
			let bundleStream = fs.createWriteStream(assetPath + '/' + 'global.bundle.js');
			const b = browserify({debug: true});
			b.add(file);
			// b.plugin('minifyify', {map: false}); //Turn on once we want to push up to prod
			b.bundle()
				.pipe(bundleStream);
			bundleStream.on('finish', () => {
				console.log('finished bundling Global JS');
			});
		});
	});

}

module.exports = build;
