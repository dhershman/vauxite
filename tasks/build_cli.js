'use strict';

const browserify = require('browserify');
const sass = require('node-sass');
const imagemin = require('imagemin');
const uglify = require('uglify-stream');
const glob = require('glob');
const path = require('path');
const fs = require('fs');

let appPath = 'core/app/';
let assetPath = 'core/public/build/js';

glob(path.resolve(appPath, '**/client.js'), {}, (err, files) => {
	files.forEach((file) => {
		let entrySplit = file.substr(file.indexOf('app/'), file.indexOf('/client')).split('/');
		let fileName = entrySplit[1] + '.' + entrySplit[2];
		let bundleStream = fs.createWriteStream(assetPath + '/' + fileName + '.bundle.js');
		const b = browserify({debug: true});
		b.add(file);
		// b.plugin('minifyify', {map: false}); //Turn on once we want to push up to prod
		b.bundle()
			.pipe(bundleStream);
		bundleStream.on('finish', () => {
			console.log('finished bundling ' + fileName);
		});
	});
});

//Global JavaScripts and Custom things that need to exist all over
glob(path.resolve('core/public', 'js/global/**.js'), {}, (err, files) => {
	files.forEach((file) => {
		let bundleStream = fs.createWriteStream(assetPath + '/' + 'global.bundle.js');
		const b = browserify({debug: true});
		b.add(file);
		// b.plugin('minifyify', {map: false}); //Turn on once we want to push up to prod
		b.bundle()
			.pipe(bundleStream);
		bundleStream.on('finish', () => {
			console.log('finished bundling Global JS');
		});
	});
});


let cssAssetPath = 'core/public/build/css';
glob(path.resolve('core/app/**/**/*.scss'), {}, (err, files) => {
	files.forEach((file) => {
		let entrySplit = file.substr(file.indexOf('app/'), file.indexOf('/index')).split('/');
		let fileLoc = entrySplit[1] + '/' + entrySplit[2];
		sass.render({
			file: file
		}, (err, result) => {
			if (!fs.existsSync(path.resolve(cssAssetPath, entrySplit[1]))) fs.mkdirSync(path.resolve(cssAssetPath, entrySplit[1]));
			if (!fs.existsSync(path.resolve(cssAssetPath, fileLoc))) fs.mkdirSync(path.resolve(cssAssetPath, fileLoc));

			fs.writeFileSync(path.resolve(cssAssetPath, fileLoc, 'index.css'), result.css);
			console.log('Compiled ' + fileLoc);
		});
	});
});

//Main website styles sass
glob(path.resolve('core/public/scss/framework.scss'), {}, (err, file) => {
	sass.render({
		file: file[0]
	}, (err, result) => {
		fs.writeFile(path.resolve(cssAssetPath, 'default.css'), result.css, (err) => {
			if (err) throw err;
			console.log('Compiled Main Theme -- Done');
		});
	});
});


let imgAssetPath = 'core/public/build/img';
//Compile loose images
imagemin([path.resolve('core/public/img/*.{jpg,png}')], path.resolve(imgAssetPath)).then(() => {
	console.log('Compiled Loose Images...');

	glob(path.resolve('core/public/img/**/*.jpg'), {}, (err, images) => {
		images.forEach((img) => {
			let entrySplit = img.substr(img.indexOf('public/'), img.indexOf('/img')).split('/');
			let fileName = entrySplit[3];
			let folderName = entrySplit[2];
			if (!fs.existsSync(path.resolve(imgAssetPath, folderName))) fs.mkdirSync(path.resolve(imgAssetPath, folderName));
			imagemin([img], path.resolve(imgAssetPath, folderName)).then(() => {
				console.log('Compiled ' + fileName);
			});
		});
	});

	glob(path.resolve('core/public/img/**/*.png'), {}, (err, images) => {
		images.forEach((img) => {
			let entrySplit = img.substr(img.indexOf('public/'), img.indexOf('/img')).split('/');
			let fileName = entrySplit[3];
			let folderName = entrySplit[2];
			if (!fs.existsSync(path.resolve(imgAssetPath, folderName))) fs.mkdirSync(path.resolve(imgAssetPath, folderName));
			imagemin([img], path.resolve(imgAssetPath, folderName)).then(() => {
				console.log('Compiled ' + fileName);
			});
		});
	});
});
