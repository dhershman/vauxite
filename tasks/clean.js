'use strict';

const rimraf = require('rimraf');

rimraf('core/public/build/css/default.css', {}, (err) => {
	if (err) throw err;
	console.log('Cleaned default.css');
});

rimraf('core/public/build/img/*', {}, (err) => {
	if (err) throw err;
	console.log('Cleaned images');
});

rimraf('core/public/build/css/*', {}, (err) => {
	if (err) throw err;
	console.log('Cleaned page css');
});

rimraf('core/public/build/js/*', {}, (err) => {
	if (err) throw err;
	console.log('Cleaned page JS');
});

