'use strict';

const sass = require('node-sass');
const glob = require('glob');
const path = require('path');
const fs = require('fs');

let assetPath = 'core/public/build/css';

//Page specific sass
function compile() {
	glob(path.resolve('core/app/**/**/*.scss'), {}, (err, files) => {
		files.forEach((file) => {
			let entrySplit = file.substr(file.indexOf('app/'), file.indexOf('/index')).split('/');
			let fileLoc = entrySplit[1] + '/' + entrySplit[2];
			sass.render({
				file: file
			}, (err, result) => {
				if (!fs.existsSync(path.resolve(assetPath, entrySplit[1]))) fs.mkdirSync(path.resolve(assetPath, entrySplit[1]));
				if (!fs.existsSync(path.resolve(assetPath, fileLoc))) fs.mkdirSync(path.resolve(assetPath, fileLoc));

				fs.writeFileSync(path.resolve(assetPath, fileLoc, 'index.css'), result.css);
				console.log('Compiled ' + fileLoc);
			});
		});
	});

	//Main website styles sass
	glob(path.resolve('core/public/scss/framework.scss'), {}, (err, file) => {
		sass.render({
			file: file[0]
		}, (err, result) => {
			fs.writeFile(path.resolve(assetPath, 'default.css'), result.css, (err) => {
				if (err) throw err;
				console.log('Compiled Main Theme -- Done');
			});
		});
	});
}

module.exports = compile;