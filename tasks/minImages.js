'use strict';

const imagemin = require('imagemin');
const path = require('path');
const glob = require('glob');
const fs = require('fs');

let assetPath = 'core/public/build/img';

//Compile sorted images
function compileImgs() {
	//Compile loose images
	imagemin([path.resolve('core/public/img/*.{jpg,png}')], path.resolve(assetPath)).then((files) => {
		console.log('Compiled Loose Images...');
	});

	glob(path.resolve('core/public/img/**/*.jpg'), {}, (err, images) => {
		images.forEach((img) => {
			let entrySplit = img.substr(img.indexOf('public/'), img.indexOf('/img')).split('/');
			let fileName = entrySplit[3];
			let folderName = entrySplit[2];
			if (!fs.existsSync(path.resolve(assetPath, folderName))) fs.mkdirSync(path.resolve(assetPath, folderName));
			imagemin([img], path.resolve(assetPath, folderName)).then((files) => {
				console.log('Compiled ' + fileName);
			});
		});
	});

	glob(path.resolve('core/public/img/**/*.png'), {}, (err, images) => {
		images.forEach((img) => {
			let entrySplit = img.substr(img.indexOf('public/'), img.indexOf('/img')).split('/');
			let fileName = entrySplit[3];
			let folderName = entrySplit[2];
			if (!fs.existsSync(path.resolve(assetPath, folderName))) fs.mkdirSync(path.resolve(assetPath, folderName));
			imagemin([img], path.resolve(assetPath, folderName)).then((files) => {
				console.log('Compiled ' + fileName);
			});
		});
	});
}

module.exports = compileImgs;